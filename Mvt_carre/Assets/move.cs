using UnityEngine;

public class move : MonoBehaviour
{
    public float speed = 10f;
    private Vector3[] targets = new Vector3[4];
    private int currentTarget = 0;

    private void Start()
    {
        targets[0] = transform.position + transform.right * speed;
        targets[1] = targets[0] + transform.up * speed;
        targets[2] = targets[1] - transform.right * speed;
        targets[3] = targets[2] - transform.up * speed;
    }

    private void Update()
    {
        if (transform.position == targets[currentTarget])
        {
            currentTarget = (currentTarget + 1) % 4;
        }
        transform.position = Vector3.MoveTowards(transform.position, targets[currentTarget], Time.deltaTime * speed);
    }
}
